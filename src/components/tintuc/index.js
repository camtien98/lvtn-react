import React, { Component } from 'react';
import '../../assests/tintuc.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
class TinTucComponent extends Component {
  render() {
    return (

      <div class="main-body">
        <hr></hr>
        <div class="main-body_frame">
          <div class="frame__title">
            <h3>ĐĂNG TIN RAO VẶT</h3>
            <div class="frame__title___des">
              <p>- Để nâng cấp chất lượng nội dung tin rao bất động sản, chúng tôi tiến hành duyệt toàn bộ tin rao đăng mới.</p>
              <p>- Tin rao đúng sẽ được duyệt chậm nhất trong vòng 9h làm việc.</p>
            </div>
          </div>

          <Row>
            <Col className="ml-5">
              <Form>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Label><b>Loại Giao Dịch</b></Form.Label>
                  <Form.Control as="select">
                    <option>Hãy chọn loại giao dịch</option>
                    <option>Cần Bán</option>
                    <option>Cần Mua</option>
                    <option>Cho Thuê</option>
                    <option>Cần Thuê</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Label><b>Danh Mục</b></Form.Label>
                  <Form.Control as="select">
                    <option>Hãy chọn danh mục</option>
                    <option>Nhà Mặt Phố</option>
                    <option>Đất Thổ Cư</option>
                    <option>Căn Hộ</option>
                    <option>Mặt Bằng</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlSelect2">
                  <Form.Label><b>Thành Phố</b></Form.Label>
                  <Form.Control as="select" >
                    <option>Hãy Chọn Tỉnh, Thành Phố</option>
                    <option>Hồ Chí Minh</option>
                    <option>Bình Dương</option>
                    <option>Đồng Nai</option>
                    <option>Long An</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea3">
                  <Form.Label><b>Quận/Huyện</b></Form.Label>
                  <Form.Control as="select"  >
                    <option>Hãy Chọn Quận, Huyện</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>Nhà Bè</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea4">
                  <Form.Label><b>Phường/Xã</b></Form.Label>
                  <Form.Control as="select"  >
                    <option>Hãy Chọn Phường, Xã</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>Nhà Bè</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea5">
                  <Form.Label><b>Hướng Nhà</b></Form.Label>
                  <Form.Control as="select"  >
                    <option>Hướng Nhà</option>
                    <option>Đông</option>
                    <option>Tây</option>
                    <option>Nam</option>
                    <option>Bắc</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea5">
                  <Form.Label><b>Đường Phố</b></Form.Label>
                  <Form.Control as="select"  >
                    <option>Đường Phố</option>
                  </Form.Control>
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlTextarea5">
                  <Form.Label><b>Diện Tích: m²</b></Form.Label>
                  <Form.Control as="textarea" rows="1" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea5">
                  <Form.Label><b>Số Tầng</b></Form.Label>
                  <Form.Control as="textarea" rows="1" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea5">
                  <Form.Label><b>Số Phòng</b></Form.Label>
                  <Form.Control as="textarea" rows="1" />
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlTextarea5" >
                  <Form.Label><b>Giá</b></Form.Label>
                  <Form.Control as="textarea" rows="1" />
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlTextarea5" >
                  <Form.Label><b>Đơn Vị</b></Form.Label>
                  <Form.Control as="select">
                    <option>tỷ</option>
                    <option>triệu</option>
                  </Form.Control>
                </Form.Group>


                <Form.Group controlId="exampleForm.ControlTextarea5">
                  <Form.Label><b>Gói Dịch Vụ Đăng Tin</b></Form.Label>
                  <Form.Control as="select">
                    <option>VIP</option>
                    <option>Thực Tế Ảo</option>
                    <option>Theo Tháng</option>
                    <option>Theo Tuần</option>
                    <option>Theo Ngày</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea5">
                  <Form.Label><b>Tổng Giá Tiền</b></Form.Label>
                  <Form.Control as="textarea" rows="1" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Thông Tin Pháp Lý</Form.Label>
                  <Form.Control as="textarea" rows="3" />
                </Form.Group>

              </Form>
              <Button variant="primary">ĐĂNG TIN</Button>{' '}
            </Col>
          </Row>
          {/* <Row>
            <Col>
              <Form>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Thông Tin Pháp Lý</Form.Label>
                  <Form.Control as="textarea" rows="3" />
                </Form.Group>
              </Form>
            </Col>
          </Row> */}
          {/* <Row>
            <Col>
              <Button variant="primary">ĐĂNG TIN</Button>{' '}

            </Col>

          </Row> */}

        </div>

      </div >
    );
  }
}
export default TinTucComponent;
