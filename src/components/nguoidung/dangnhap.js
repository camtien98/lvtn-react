import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBCard, MDBInput } from 'mdbreact';
import { Container } from "react-bootstrap";
import axios from 'axios';

class DangNhapComponent extends Component {
  constructor(){
    super();
    this.state={
      email:null,
      password:null,
      login:false,
      store:null
    }

  }
  login()
  {
    const{login}=this.state;

    const{email ,password}=this.state;
    console.log(email);

      fetch('http://127.0.0.1:8000/api/dangnhapkhachhang/',{
        method :"POST",
        headers: { 'Content-Type': 'application/json' },
        body:JSON.stringify(this.state)
      }).then((response)=>{
        response.json().then((result)=>{
          console.warn("result",result);
          localStorage.setItem('login',JSON.stringify({
            login:result.success,
            token:result.token,
          }));
        });

      })
      let store =JSON.parse(localStorage.getItem('login'));

      if(store && store.login ==true)
      {
        this.setState({login:true});
      }
  }

  render(){

    return (


      <MDBContainer>
      <hr></hr>
      {this.state.login && <Redirect to='/dangtin' /> }
        <MDBRow>
        <MDBCol md="9">
        <form>
          <p className="h5 text-left mb-4">Nếu bạn đã là thành viên xin mời đăng nhập. Nếu chưa, xin mời bạn đăng ký tài khoản</p>
          <div className="grey-text">
            <MDBInput label="Họ tên" icon="user" group type="text" validate error="wrong"
              success="right" />

            <MDBInput label="Email" icon="envelope" group type="text" validate
              error="wrong" success="right" />
            <MDBInput label="Mật Khẩu" icon="lock" group type="password" validate />
            <MDBInput label="Nhập lại mật khẩu" icon="lock" group type="password" validate />

            <MDBInput label="Số điện thoại" icon="envelope" group type="email" validate error="wrong"
              success="right" />

            <MDBInput label="Địa chỉ" icon="user" group type="text" validate error="wrong"

              success="right" />
                <MDBInput label="CMND" icon="user" group type="text" validate error="wrong"

            success="right" />
          </div>
          <div className="text-center">
            <MDBBtn color="primary">ĐĂNG KÝ THÀNH VIÊN</MDBBtn>
          </div>
        </form>
      </MDBCol>
        <MDBCol>
        <Container className="clearfix justify-content-right ">
          <MDBCol md='10'>
            <MDBCard
              className='card-image'
              style={{
                backgroundImage:
                  'url(https://mdbootstrap.com/img/Photos/Others/pricing-table7.jpg)',
                width: '24rem'

              }}
            >
              <div className='text-white rgba-stylish-strong py-5 px-5 z-depth-4'>
                <div className='text-left'>
                  <h3 className='white-text mb-5 mt-4 font-weight-bold'>
                    <strong>ĐĂNG NHẬP</strong>

                  </h3>
                </div>
                <MDBInput
                  label='Nhập email'
                  group
                  type='text'
                  name='email'
                  validate
                  labelClass='white-text'
                  onChange={(event)=>{this.setState({email:event.target.value})}}
                />
                <MDBInput
                  label='Nhập Mật Khẩu'
                  group
                  name='password'
                  type='password'
                  validate
                  labelClass='white-text'
                  onChange={(event)=>{this.setState({password:event.target.value})}}

                />

                <MDBRow className='d-flex align-items-center mb-4'>
                  <div className='text-center mb-3 col-md-12'>
                    <MDBBtn
                      color='success'
                      rounded
                      type='button'
                      className='btn-block z-depth-1'
                      onClick={()=>{this.login()}}
                    >
                   { /*<Link to ="/dangtin">ĐĂNG NHẬP</Link>*/ }
                   Đăng Nhập
                    </MDBBtn>
                  </div>
                </MDBRow>

              </div>
            </MDBCard>
          </MDBCol>
          </Container>
        </MDBCol>

        </MDBRow>
        <hr></hr>

      </MDBContainer>
    );
  }
}


export default DangNhapComponent ;
