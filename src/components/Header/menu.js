import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link } from 'react-router-dom';

function Menu() {
        return (
                <>
                        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                                <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                                        <Nav className="d-flex align-items-center">
                                                <NavDropdown title="MUA" id="collasible-nav-dropdown">
                                                <Link className="text-black" to="/danhsach">Danh sách tin</Link>
                                                        <NavDropdown.Item href="#action/3.1">Căn Hộ</NavDropdown.Item>
                                                        <NavDropdown.Item href="#action/3.2">Nhà</NavDropdown.Item>
                                                        <NavDropdown.Item href="#action/3.3">Đất Nền</NavDropdown.Item>
                                                        <NavDropdown.Item href="#action/3.4">Mặt Bằng</NavDropdown.Item>
                                                </NavDropdown>
                                                <NavDropdown title="THUÊ" id="collasible-nav-dropdown">
                                                        <NavDropdown.Item href="#action/3.1">Căn Hộ</NavDropdown.Item>
                                                        <NavDropdown.Item href="#action/3.2">Nhà</NavDropdown.Item>
                                                        <NavDropdown.Item href="#action/3.3">Đất Nền</NavDropdown.Item>
                                                        <NavDropdown.Item href="#action/3.4">Mặt Bằng</NavDropdown.Item>
                                                </NavDropdown>
                                                <Nav.Link href="#home">TOUR 360°</Nav.Link>
                                                <Nav.Link href="#features">BẢN ĐỒ</Nav.Link>
                                                <Nav.Link href="#pricing">TIN TỨC</Nav.Link>
                                                <Nav.Link href="#pricing">HỎI ĐÁP</Nav.Link>
                                                
                                                <Nav.Link >
                                                        <Button variant="outline-danger"><Link className="text-white" to="/dangnhap">ĐĂNG TIN</Link></Button>                                                        
                                                </Nav.Link>
                                               
                                                <Nav.Link >
                                                <Button variant="outline-success"><Link className="text-white" to="/timkiem">Tìm kiếm</Link>
                                                </Button>
                                                </Nav.Link>

                                        </Nav>
                                </Navbar.Collapse>
                                
                        </Navbar>
                </>
        );
}


export default Menu;