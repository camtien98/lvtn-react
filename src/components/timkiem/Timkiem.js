import React, { Component } from 'react';
import { Container, Button } from 'react-bootstrap';

class Timkiem extends Component{

  render() {
    return(

        <>
        <Container className="clearfix justify-content-center ">
          <hr></hr>
        <div className="click-closed" />
        {/*/ Form Search Star /*/}
        <div className="box-collapse">
          <div className="title-box-d">
            <h3 className="title-d">Tìm Kiếm</h3>
          </div>
          
          <div className="box-collapse-wrap form">
            <form className="form-a">
              <div className="row">
                <div className="col-md-12 mb-2">
                  <div className="form-group">
                    <label htmlFor="Type">Tìm kiếm theo tên</label>
                    <input type="text" className="form-control form-control-lg form-control-a" placeholder="Tên dự án" />
                  </div>
                </div>
                <div className="col-md-6 mb-2">
                  <div className="form-group">
                    <label htmlFor="Type">Hình Thức</label>
                    <select className="form-control form-control-lg form-control-a" id="Type">
                      <option>All Type</option>
                      <option>For Rent</option>
                      <option>For Sale</option>
                      <option>Open House</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 mb-2">
                  <div className="form-group">
                    <label htmlFor="Type">Loại</label>
                    <select className="form-control form-control-lg form-control-a" id="Type">
                      <option>All Type</option>
                      <option>For Rent</option>
                      <option>For Sale</option>
                      <option>Open House</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 mb-2">
                  <div className="form-group">
                    <label htmlFor="city">Thành Phố</label>
                    <select className="form-control form-control-lg form-control-a" id="city">
                      <option>All City</option>
                      <option>Alabama</option>
                      <option>Arizona</option>
                      <option>California</option>
                      <option>Colorado</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 mb-2">
                  <div className="form-group">
                    <label htmlFor="bedrooms">Quận/Huyện</label>
                    <select className="form-control form-control-lg form-control-a" id="bedrooms">
                      <option>Any</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 mb-2">
                  <div className="form-group">
                    <label htmlFor="garages">Phường/Xã</label>
                    <select className="form-control form-control-lg form-control-a" id="garages">
                      <option>Any</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                      <option>04</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 mb-2">
                  <div className="form-group">
                    <label htmlFor="bathrooms">Diện Tích</label>
                    <select className="form-control form-control-lg form-control-a" id="bathrooms">
                      <option>Any</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 mb-2">
                  <div className="form-group">
                    <label htmlFor="price">Gía Cả</label>
                    <select className="form-control form-control-lg form-control-a" id="price">
                      <option>Unlimite</option>
                      <option>$50,000</option>
                      <option>$100,000</option>
                      <option>$150,000</option>
                      <option>$200,000</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-12">
                  <Button type="submit" variant="success">Tìm Kiếm</Button>
                </div>
              </div>
            </form>
          </div>
          </div>
          <hr></hr>
          </Container>
        </>


    );
  }
}
export default Timkiem;