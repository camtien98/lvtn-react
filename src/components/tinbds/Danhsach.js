import React, { Component } from 'react';
import { Card, ListGroupItem, ListGroup, Container, Row, Col, Image, Pagination } from 'react-bootstrap';
import { Link } from 'react-router-dom';
function Danhsach() {
    return (
        <Container className="clearfix justify-content-center ">

            <hr></hr>
            <hr></hr>


            <Card>
                <Row>
                    <Col xs={3} md={4}>
                        <Card.Img variant="top"
                            src="https://nhadat24h.net/Upload/User/DangTin/2020/Images/282635/5b321f6c-7e6b-4bdf-8c95-4cb1ec858c8a.jpg" />
                    </Col>
                    <Col xs={8} md={4}>

                        <Card.Body>
                            <Card.Text>
                                <h3><Link to="/chitiet">Tiêu đề tin</Link></h3>
                            </Card.Text>
                            <Card.Text>
                                <h4>Nội Dung Mô Tả:</h4>
                            </Card.Text>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk
                                of the card's content.
                            </Card.Text>
                        </Card.Body>
                    </Col>
                </Row>
            </Card>
            <hr></hr>

            <Card>
                <Row>
                    <Col xs={3} md={4}>
                        <Card.Img variant="top"
                            src="https://nhadat24h.net/Upload/User/DangTin/2020/Images/282635/5b321f6c-7e6b-4bdf-8c95-4cb1ec858c8a.jpg" />
                    </Col>
                    <Col xs={8} md={4}>

                        <Card.Body>
                            <Card.Text>
                                <h3>Tiêu đề tin</h3>
                            </Card.Text>
                            <Card.Text>
                                <h4>Nội Dung Mô Tả:</h4>
                            </Card.Text>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk
                                of the card's content.
                            </Card.Text>
                        </Card.Body>
                    </Col>
                </Row>
            </Card>
            <hr></hr>

            <Card>
                <Row>
                    <Col xs={3} md={4}>
                        <Card.Img variant="top"
                            src="https://nhadat24h.net/Upload/User/DangTin/2020/Images/282635/5b321f6c-7e6b-4bdf-8c95-4cb1ec858c8a.jpg" />
                    </Col>
                    <Col xs={8} md={4}>

                        <Card.Body>
                            <Card.Text>
                                <h3>Tiêu đề tin</h3>
                            </Card.Text>
                            <Card.Text>
                                <h4>Nội Dung Mô Tả:</h4>
                            </Card.Text>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk
                                of the card's content.
                            </Card.Text>
                        </Card.Body>
                    </Col>
                </Row>
            </Card>
            <hr></hr>

            <Card>
                <Row>
                    <Col xs={3} md={4}>
                        <Card.Img variant="top"
                            src="https://nhadat24h.net/Upload/User/DangTin/2020/Images/282635/5b321f6c-7e6b-4bdf-8c95-4cb1ec858c8a.jpg" />
                    </Col>
                    <Col xs={8} md={4}>

                        <Card.Body>
                            <Card.Text>
                                <h3>Tiêu đề tin</h3>
                            </Card.Text>
                            <Card.Text>
                                <h4>Nội Dung Mô Tả:</h4>
                            </Card.Text>
                            <Card.Text>
                                Some quick example text to build on the card title and make up the bulk
                                of the card's content.
                            </Card.Text>
                        </Card.Body>
                    </Col>
                </Row>
            </Card>
            <hr></hr>

            <Pagination>
               
                <Pagination.Prev />
                <Pagination.Item active>{1}</Pagination.Item>
                <Pagination.Item>{2}</Pagination.Item>
                <Pagination.Item>{3}</Pagination.Item>
                <Pagination.Item>{4}</Pagination.Item>
                <Pagination.Item>{5}</Pagination.Item>
                <Pagination.Item disabled>{9}</Pagination.Item>

                <Pagination.Ellipsis />
                <Pagination.Item>{10}</Pagination.Item>
                <Pagination.Next />
                
            </Pagination>

        </Container>

    );
}

export default Danhsach;