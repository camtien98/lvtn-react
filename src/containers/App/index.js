/**
 *
 * App
 *
 */

import React from 'react';
import { Helmet } from 'react-helmet';

import { Redirect, Route, Switch } from 'react-router-dom';
import Home from '../Home/Loadable';
import withMainLayout from '../layouts/MainLayout';
import Tintuc from '../Page/Tintuc';
import Timkiem from '../Page/Timkiem';
import Danhsach from '../../components/tinbds/Danhsach';
import Chitiettin from '../../components/tinbds/Chitiettin';
import DangNhap from '../Page/Dangnhap';

export function App() {
  return (
    <>
      <Helmet>
        <title>App</title>
        <meta name="description" content="Description of App" />
      </Helmet>
      <>
        <Switch>
          <Route exact path="/" component={withMainLayout(Home)} />
          <Route path="/timkiem" exact component={withMainLayout(Timkiem)}/>;

        <Route path="/danhsach" exact component={withMainLayout(Danhsach)}/>;

        <Route path="/chitiet" exact component={withMainLayout(Chitiettin)}/>;

        <Route path="/dangnhap" exact component={DangNhap}>
</Route>;

        <Route path="/dangtin" exact component={Tintuc}/>
          <Redirect to="/" />
        </Switch>
      </>
    </>
  );
}

export default App;
