import React from 'react';
import styled from 'styled-components';
import theme from 'utils/theme';
import { Button } from 'reactstrap';
import SchoolBagIcon from 'components/Icons/SchoolBag';

const Wrapper = styled.div``;

const PromotionTitle = styled.div``;

const SearchWrapper = styled.div``;
const LocationSelector = styled.div``;

const SearchButton = styled(Button)`
  .span {
  }

  .fa {
  }
`;

// eslint-disable-next-line react/prefer-stateless-function
class Banner extends React.Component {
  render() {
    return (
      <Wrapper>
        <PromotionTitle>1,814 IT Jobs For Chất Developers</PromotionTitle>

        <SearchWrapper>
          {/* <Input /> */}
          <LocationSelector />
          <SearchButton>
            <span>Search</span>
            <SchoolBagIcon />
          </SearchButton>
        </SearchWrapper>
      </Wrapper>
    );
  }
}

export default Banner;

// scss
// styled
