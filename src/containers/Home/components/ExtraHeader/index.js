import PropTypes from 'prop-types';
import styled from 'styled-components';
import React from 'react';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import SearchInput from 'components/Form/SearchInput';
import DropdownInput from 'components/Form/DropdownInput';
import { Button } from 'reactstrap';
import {
  CenterContainer,
  FullWidthContainer,
} from 'components/Layouts/Container';

const SearchTool = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  .portal-input--search-box {
    height: 40px !important;
    width: 300px !important;
  }
  .portal-input--select {
    margin: 0 10px;
    .form-group {
      margin-bottom: 0 !important;
      select {
        height: 40px !important;
      }
    }
  }
`;

const StyledCenterContainer = styled(CenterContainer)`
  margin: 0 auto;
  padding: 20px;
`;

const StyledFullWidthContainer = styled(FullWidthContainer)`
  padding: 20px;
  height: 300px;
  background: gray;
  margin-top: 50px;
`;

const StyledButton = styled(Button)`
  height: 40px;
`;

const Title = styled.h1`
  margin-bottom: 20px;
`;

function ExtraHeader() {
  // const submitFormDebounce = React.useCallback(
  //   _.debounce(() => submitForm(), 500),
  //   [submitForm],
  // );

  return (
    <StyledFullWidthContainer>
      <StyledCenterContainer>
        <Title>
          <FormattedMessage {...messages.extraHeaderTitle} />
        </Title>
        <SearchTool>
          <SearchInput
          // value={values.search_text}
          // onChange={e => {
          //   // handleChange('search_text')(e);
          //   // submitFormDebounce();
          // }}
          // placeholder={'Key Skill (Java, IOS, ...)'}
          // onBlur={handleBlur('search_text')}
          />
          <DropdownInput
            options={[
              { id: '001', name: 'Ho Chi Minh' },
              { id: '002', name: 'Ha noi' },
            ]}
          />
          <StyledButton color="danger"> Search</StyledButton>
        </SearchTool>
      </StyledCenterContainer>
    </StyledFullWidthContainer>
  );
}

ExtraHeader.propTypes = {
  // values: PropTypes.any,
  // handleChange: PropTypes.any,
  // handleBlur: PropTypes.any,
  // submitForm: PropTypes.any,
  // setFieldValue: PropTypes.any,
};

export default React.memo(ExtraHeader);

//  // errors,
//  values,
//  handleChange,
//  // submitCount,
//  setFieldValue,
//  submitForm,
//  // handleSubmit,
//  handleBlur,
//  // resetForm,