import { withFormik } from 'formik';
import { compose } from 'redux';
import _ from 'lodash';

import * as campaignActions from 'containers/Campaign/actions';

import Header from '.';

const mapPropsToValues = props => ({
  text: _.get(props.params, 'text', ''),
  status: _.get(props.params, 'status', ''),
});

export default compose(
  withFormik({
    enableReinitialize: true,
    mapPropsToValues,
    handleSubmit: (values, formik) => {
      const { props } = formik;
      const { dispatch } = props;
      dispatch(campaignActions.getCampaignHistory(values));
    },
  }),
)(Header);
